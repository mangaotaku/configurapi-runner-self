import { Request, Response } from 'configurapi';
import { IncomingMessage, ServerResponse } from 'http';
import { parse } from 'url';
import { v4 } from 'uuid';

export class HttpAdapter {
    static write(response: Partial<Response & { jsonReplacer: any }>, serverResponse: ServerResponse)
    {
        serverResponse.writeHead(response.statusCode, response.headers);

        let body: Buffer | String;

        if(response.body instanceof Buffer)
        {
            body = response.body;
        }
        else if(response.body instanceof String)
        {
            body = response.body;
        }
        else if(response.body instanceof Object)
        {
            body = JSON.stringify(response.body, response.jsonReplacer);
        }
        else if(response.body instanceof Number)
        {
            body = response.body + '';
        } 
        else 
        {
            body = response.body;
        }
        serverResponse.end(body || '');
    }

    static async toRequest(incomingMessage: Partial<IncomingMessage & Request>)
    {
        let request = new Request('');
        
        request.method = incomingMessage.method?.toLowerCase() || '';
        request.headers = incomingMessage.headers;

        if (incomingMessage.url) {
            let url = parse(incomingMessage.url, true);
            request.payload = undefined as any;
            request.query = url.query as any;
            request.path = url.pathname as any;
            request.pathAndQuery = url.href;

        } else {
            request.headers = incomingMessage.headers || {};
            request.headers['correlation-id'] = request.headers['correlation-id'] || v4();
            request.payload = incomingMessage.payload;
            request.query = incomingMessage.query || {} as any;
            request.name = incomingMessage.name;
            request.path = '';
            request.pathAndQuery = '';
            return request;
        }

        let data: any[] = [];
        return new Promise((resolve, reject) => 
        {    
            incomingMessage.on('data', function(chunk) {
                data.push(chunk);
            })
            .on('end', function() {
                if(data.length <= 0) {
                    request.payload = undefined as any;
                    return resolve(request);
                }

                request.payload = Buffer.concat(data).toString();

                if(request.payload && 'content-type' in request.headers)
                {
                    let contentType: string = request.headers['content-type'] as string;

                    if(contentType.startsWith('text/'))
                    {
                        request.payload = request.payload.toString();
                    }
                    else if(contentType.startsWith('application/json'))
                    {
                        try
                        {
                            if(request.payload)
                            {
                                request.payload = JSON.parse(request.payload);  
                            }
                        }
                        catch(err)
                        {
                            reject(err);
                        } 
                    }
                }

                resolve(request);
            })
            .on('error', function(err) {
                reject(err);
            });
        });
    }
};
