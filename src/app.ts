#!/usr/bin/env node
import { Command } from 'commander';
import { readFileSync } from 'fs';
import oneliner = require('one-liner');

import { HttpRunner } from '.';

///////////////////////////////////
// Utility functions
///////////////////////////////////
function format(prefix: string, str: string)
{
    let time = new Date().toISOString();
    return `${time} ${prefix}: ${oneliner(str)}`;
}
function logTrace(s: string)
{
    console.log(format('Trace', s));
}
function logError(s: string)
{
    console.error(format('Error', s));
}
function logDebug(s: string)
{
    console.log(format('Debug', s));
}

///////////////////////////////////
// Process arguments
///////////////////////////////////
let program = new Command()
    .option('-p, --port [number]', 'Port number')
    .option('--s-port [number]', 'Secure port number')
    .option('-f, --file [path]', 'Path to the config file')
    .option('--key [path]', 'Path to the private key file (.key)')
    .option('--cert [path]', 'Path to the certificate file (.pem)')
    .option('-k, --keep-alive [number]', 'The number of milliseconds of inactivity a server needs to wait. Default to 0 which disable the keep alive behavior')
    .option('-c, --on-connect [boolean]', 'Execute connect handler on connect.')
    .option('-d, --on-disconnect [boolean]', 'Execute disconnect handler on disconnect.')
    .parse(process.argv);

const options = program.opts();
let port = options.port ? options.port : 8080;
let sPort = options.sPort ? options.sPort : 8443;
let configPath = options.file ? options.file : 'config.yaml';
let keepAliveTimeout = options.keepAlive ? options.keepAlive : (process.env.CONFIGURAPI_RUNNER_SELF_KEEP_ALIVE || 0);
let key = options.key ? readFileSync(options.key) : undefined;
let cert = options.cert ? readFileSync(options.cert) : undefined;
let onConnect = options?.onConnect || false;
let onDisconnect = options?.onDisconnect || false;

///////////////////////////////////
// Start the API
///////////////////////////////////
let app = new HttpRunner({
    port,
    sPort,
    configPath,
    keepAliveTimeout,
    key,
    cert,
    onConnect,
    onDisconnect,
});
app.on('error', (s) => {logError(s);});
app.on('debug', (s) => {logDebug(s);});
app.on('trace', (s) => {logTrace(s);});
app.run();