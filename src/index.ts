import { Config, ErrorResponse, Event, Request, Service } from 'configurapi';
import { EventEmitter } from 'events';
import { createServer, IncomingMessage, ServerResponse } from 'http';
import { createServer as createHttpsServer } from 'https';
import { v4 } from 'uuid';
import { RawData, Server, WebSocket } from 'ws';

import { HttpAdapter } from './httpAdapter';

require('dotenv').config();

class WebsocketResponse extends ServerResponse {
    data = "";
    constructor(req: IncomingMessage, onEnd: (args) => any) {
        super(req);
        this.end = onEnd;
    }
}

interface IHttpRunner {
    port?: number;
    sPort?: number;
    configPath: string;
    service: Service;
    keepAliveTimeout: number;
    key: Buffer;
    cert: Buffer;
    connections: object;
    onConnect: boolean;
    onDisconnect: boolean;
}

export class HttpRunner extends EventEmitter {
    port?: number;
    sPort?: number;
    configPath: string;
    service: Service;
    keepAliveTimeout: number;
    key: Buffer;
    cert: Buffer;
    connections: object;
    onConnect: boolean;
    onDisconnect: boolean;

    constructor(options?: Partial<IHttpRunner>) {
        super();
        this.port = options?.port || 8000;
        this.configPath = options?.configPath || 'config.yaml';
        this.keepAliveTimeout = options?.keepAliveTimeout || 0;
        this.key = options?.key;
        this.cert = options?.cert;
        this.sPort = options?.sPort || 8443;
        this.onConnect = options?.onConnect;
        this.onDisconnect = options?.onDisconnect;
        this.connections = {};
    }

    run() {
        let config = Config.load(this.configPath);

        this.service = new Service(config);
        this.service.on("trace", (s) => this.emit("trace", s));
        this.service.on("debug", (s) => this.emit("debug", s));
        this.service.on("error", (s) => this.emit("error", s));

        if(this.cert && this.key && this.sPort)
        {
            let server = createHttpsServer({key: this.key, cert: this.cert}, (req, resp) => {
                this._requestListener(req as any, resp);
            });

            server.keepAliveTimeout = this.keepAliveTimeout;
            server.listen(this.sPort);

            this.emit("trace", "Secure Server is listening...");
        }
        
        let server = createServer((req, resp) => {
            this._requestListener(req as any, resp);
        });

        server.keepAliveTimeout = this.keepAliveTimeout;
        let wss = new Server({server: server});
        
        wss.on('connection', (ws: WebSocket, wsRequest: IncomingMessage) => {
            ws['id'] = v4();
            this.connections[ws['id']] = ws;

            if(this.onConnect) {
                let newRequest = new Request('');
                newRequest.name = 'connect';
                newRequest.headers = ws.headers;
                this._requestListener(
                    newRequest as any,
                    new WebsocketResponse(newRequest, (requestResponse) => {
                        if(requestResponse instanceof ErrorResponse) {
                            ws.send(requestResponse, { binary: false });
                            ws.close(requestResponse.statusCode);
                        }
                    }));
            }

            ws.on("message", (req: RawData, isBinary: boolean) => {
                let re = JSON.parse(req.toString());
                re.id = v4();
                this._requestListener(re, 
                    new WebsocketResponse(re, (x) => {
                        ws.send(x, { binary: false });
                    })
                );
            });

            ws.on("close", (ws: WebSocket, code: number, reason: Buffer) => {
                if(this.onDisconnect) {
                    ws['name'] = 'disconnect';
                    this._requestListener(
                        ws as any, 
                        new WebsocketResponse(undefined, (x) => {
                            ws.send(x, { binary: false });
                        })
                    );
                }
            });

            ws.onerror = function () {
                console.log("Some Error occurred")
            }
        });
        server.listen(this.port);
        this.emit("trace", "Server is listening...");
    }

    async _requestListener(incomingMessage: Partial<IncomingMessage & Request>, serverResponse: ServerResponse) {
        try {
            let event = new Event(await HttpAdapter.toRequest(incomingMessage) as Request);

            await this.service.process(event);

            HttpAdapter.write(event.response, serverResponse);
        }
        catch (error) {
            let response = new ErrorResponse(error, error instanceof SyntaxError ? 400 : 500);

            HttpAdapter.write(response, serverResponse);
        }
    }
};
